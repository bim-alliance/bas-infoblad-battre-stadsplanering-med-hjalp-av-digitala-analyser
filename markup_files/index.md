![Bild 1](media/1.jpeg)

**Vindsimuleringar är ett viktigt digitalt redskap för att testa hur olika förslag på byggnaders storlek, läge och höjd påverkar hur vinden rör sig i ett område. Arbete med vindsimuleringar gör det möjligt att successivt närma sig bästa lösningen för hur en stadsdel ska utformas**

# Bättre stadsplanering medhjälp av digitala analyser

> ##### Vid stadsplanering är det mycket värdefullt att i ett tidigt skede använda sig av vindsimuleringar för att skapa bra förutsättningar för att uppnå bästa lösningar för ett område. Tack vare digitala verktyg blir det möjligt att genomföra en iterativ process.

NÄR WHITE ARKITEKTER SKULLE DELTA I UTFORMNINGEN av stadsplan för etapp 3 av Gävle Strand var kraven från beställaren Gävle kommun bland annat att området ska vara en långsiktigt hållbar stadsdel med bra klimat och bra energilösningar. Den sista
etappen av projektet att omvandla de gamla hamnkvarteren till ett bostadsområde för cirka 2 400 invånare, består av det område som ligger längst ut mot havet och som är mycket utsatt för vind och hårt väder.
​	– För att utveckla området, få rätt volymer på byggnaderna, ett bra gatunät och bra kvartersstrukturer måste vi självklart förhålla oss till vädret och klimatet, säger Dermot Farrelly, miljöspecialist på White arkitekter. Vi ville skapa bra förutsättningar
för att byggnaderna skulle kunna få bra energiprestanda och ett bra mikroklimat för dem som rör sig i området.
​	De två första etappernas områden utgör en fortsättning av staden ut mot havet och har raka gatunät. Men så gick det inte att fortsätta i den avslutande etappen. Vinden måste stoppas och lugna platser skapas i området. Om folk dessutom ska
lockas från stadskärnan ut till havet måste kvarteren vara välkomnande och skyddade.
​	– Vi måste utgå från att detta är unik plats som behöver en unik lösning. Då måste vi tidigt göra en vindanalys för att säkerställa en bra grund för det fortsatta arbetet med att ta fram ett förslag till stadsplan för detta förhållandevis ganska utsatta
område.
​	En vind rose för området, som visar vindhastighet och vindriktning under årets olika delar, köptes in från SMHI. Denna vind rose kopplades till programmet Sim CFD där en CFDmodell byggdes upp. CFD står för Computational fluid dynamics (beräkningsströmningsdynamik).
​	Arbetsgruppen tog fram olika idéer om gatunät, huskroppar, volymer och kvartersstorlek i Sketchup som sedan utsattes för vindsimuleringar. I området finns en silo som skulle bevaras och utifrån dess stora volym var det viktigt hur den
kan samverka med kringliggande huskroppar. Det skulle även skapas ett lugnt och välkomnande torg ut mot Gavleån. 
​	Under den iterativa processen med att skapa bästa plan för området testades hur olika förslag på byggnaders storlek, läge och höjd påverkade vindutsattheten. Huskroppar användes för att skapa skyddade platser.
​	– Vindsimuleringarna spelade mycket stor roll för hur vi utformade området, det var en av de viktigaste faktorerna för utformning av gatunät och placeringar av huskroppar. På den korta tid vi hade på oss och med den budget vi förfogade över hade vi aldrig klarat detta utan hjälp av modelbaserade digitala verktyg. Den iterativa processen hade inte gått att genomföra.

DET VAR INTE MÖJLIGT ATT ARBETA MED GRÖNSKA i vindsimulering till tävlingsförslaget men det går utmärkt att lägga in både solitära träd, alléer, häckar buskar och annan form av grönska i simuleringsprogrammet och se vilken skillnad det blir i vindpåverkan i ett område. Helhetsperspektivet blir ett steg större.
​	– Det är önskvärt att i ett tidigt skede även väva in grönskan i simuleringarna eftersom växtlighet har en stor påverkan på vind i den urbana miljön och varje typ av vegetation har med sin speciella egenskap möjlighet att skapa vindskydd. Vilken typ man väljer beror mest på hur mycket plats som är tillgänglig.

ATT ARBETA MED VINDSIMULERINGAR tar varken mycket tid eller är kostsamt och Dermot Farrelly menar att arbetssättet borde användas i de flesta stadsplaneringsprojekt för att skapa en bra grund för det fortsatta arbetet. Arbetet förutsätter att någon, vanligtvis en ingenjör, är insatt i och kan hantera programmet.
​	Josef Wårdsäter, planarkitekt på Samhällsbyggnad Gävle, är också mycket positiv till arbete med vindsimuleringar.
​	– Vindanalysen hade stor betydelse för hur vårt slutgiltiga förslag till etapp 3 ser ut. Platsen har ett mycket utsatt läge och det är en stor bonus att man med hjälp av simuleringar kan visa att det går att skapa väderskyddande lägen. Jag tror inte det varit möjligt att få fram ett så bra förslag utan att göra en vindanalys.

![Bild 2](media/2.jpeg)

**Det färdiga förslaget till hur etapp 3 av projektet att omvandla de gamla hamnkvarteren i Gäcle till en ny stadsdel, ska utformas.** 

DERMOT FARRELLY OCH HANS MEDARBETARE har summerat några viktiga aspekter när det gäller klimatanpassning av städer och där vindsimuleringar har stort värde:

• FOKUSERAPÅ DE ÖVERVÄGANDE vindriktningarna med den största vindhastigheten. Här finns den största potentialen att förbättra mikroklimatet. Vindriktning är mycket beroende av platsen. Geografiskt läge bestämmer ofta vilken vindriktning som är
relevant. Den övervägande vindriktningen förändras något under dygnet och vid olika årstider och därför är det viktigt att bestämma vilka tidpunkter som är viktiga just på den gällande platsen.

• VINDHASTIGHETEN HAR STOR PÅVERKAN på hur vind rör sig genom ett område. En plats som används året runt, även vid dåligt väder, behöver man simulera med högre vindhastigheter. Om det gäller en plats som används mest vid bra väder räcker det
att simulera platsen med en låg vindhastighet. Använd lokala siffror för vindhastighet om sådana finns.

• TERMISK KOMFORT ÄR BEROENDE av mer än bara vinden och påverkas även av lufttemperatur, strålning, nederbörd och luftfuktighet. De två faktorer som man kan ändra genom gestaltning och planering och som har stor påverkan på termisk komfort är vind och strålning. Därför bör man fokusera på dessa två faktorer. Vad gäller termisk komfort är det viktigt att ta hänsyn till användarens aktivitetsnivå: används platsen mer aktivt eller passivt?

• VISSA PLATSER ÄR MER RELEVANTA vad gäller att skapa termisk komfort, till exempel mötesplatser, vistelseplatser, byggnaders entréer och väntzoner. Fokusera därför vindanalyserna på dessa områden